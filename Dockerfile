FROM ubuntu:18.04
LABEL maintainer "Ederson Chimbida (chimbida@gmail.com)"

RUN apt-get clean && \
  apt-get -y update && \
  apt-get install -y --no-install-recommends \
    python3-opencv python3-pip python3-setuptools tzdata && \
  echo "America/Sao_Paulo" | tee /etc/timezone && \
  ln -sf /usr/share/zoneinfo/America/Sao_Paulo /etc/localtime && \
  dpkg-reconfigure -f noninteractive tzdata && \ 
  apt-get -y autoremove && \
  apt-get clean autoclean && \
  rm -rf /var/lib/{apt,dpkg,cache,log}/ && \
  rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

COPY . /tmp
WORKDIR /tmp
RUN pip3 install --no-cache-dir -r /tmp/requirements.txt &&\
  rm -rf /tmp/* 

